# Piano Queue

Piano Queue is a python package that can be used to interface with  
a broker/queueing service to enqueue jobs, check their status, and get their results.

It currently only supports Redis as the broker/queueing service provider, but additional 
services can be added by simply implementing the base __PianoQ_ class.

### Tech

Piano Q uses some open source projects to function:

* [Python] - programming language
* [Redis] - in-memory data structure store, used as a database, cache, and message broker
* [Python-RQ] - Python library for queueing jobs and processing them in the background with workers

### Compatibility

Piano Q is compatible with both Python 2.7, and Python 3

### Installation

```sh
pip install git+https://bitbucket.org/evidentli/pyano-q
```

### Use

The following example is in the context of a Flask app:

##### Initialisation

```python
from flask import Flask
from piano_q import get_q

app = Flask(__name__)

piano_q = get_q("my-queue-name", "redis", 6379)
```

You can also supply the following keyword arguments:

| Name                  | Default | Description                                                                                                         |
|-----------------------|---------|---------------------------------------------------------------------------------------------------------------------|
| default_timeout       | 300     | Execution timeout, in seconds, for each job (i.e. if a job takes longer than this to execute, it will timeout       |
| default_result_expiry | 500     | Execution result expiry, in seconds, for each job (i.e. how long the result is retained, after which it is deleted) |

##### General Usage

```python
from flask import request, jsonify
from my_app import app, piano_q, long_running_func

@app.route('/api/execute/<exec_id>', methods=["POST", "GET"])
def execute(exec_id):
    job_id = exec_id
    status_exec = {"status": "executing"}
    if piano_q.is_pending(job_id):
        return jsonify(status_exec)
    
    if request.method == "GET":
        response = {"status": None}
        result = piano_q.get_result(job_id)
        if result:
            response.update(result)
        return jsonify(response)
    
    if piano_q.enqueue(long_running_func, job_id=job_id, args=(exec_id)):
        return jsonify(status_exec)
    # ... return/raise http error as enqueueing failed
```

The following parameters can be supplied to the `enqueue` methods:

| Name        | Type       | Default | Description                                                                               |
|-------------|------------|---------|-------------------------------------------------------------------------------------------|
| description | `str`      | `None`  | Additional description to enqueued jobs                                                   |
| meta        | `dict`     | `None`  | A dictionary holding custom status information on this job                                |
| on_success  | `Callable` | `None`  | Run a function after a job completes successfully                                         |
| on_failure  | `Callable` | `None`  | Run a function after a job fails                                                          |
| depends_on  | `any`      | `None`  | Specifies another job (or list of jobs) that must complete before this job will be queued |

> **NOTE:** the callback parameters cannot be compiled/obfuscated (e.g. with Cython)

### Deployment

See the [redis setup guide](REDIS_SETUP.md) for more information on how to deploy and integrate 
the code/configuration for the required Redis workers to function.

License
----
MIT

## Authors

* [Evidentli](https://evidentli.com/)


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [Python]: <https://www.python.org/>
   [Redis]: <https://redis.io/>
   [Python-RQ]: <https://python-rq.org/>

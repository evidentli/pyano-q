# Setting up Redis

A quick guide on how to get a redis environment setup and working so that you can integrate piano-q with your code.

### Pre-requisites:

* [Docker](https://www.docker.com/)
* [Python](https://www.python.org/)


### Installation

See [README](README.md).

### Redis Queue (RQ) Server

See [here](https://hub.docker.com/_/redis/) for more information on running and configuring the redis docker image/container.

```sh
docker pull redis
docker run -d -p 6379:6379 --name piano-redis redis  
```


### RQ Worker

To start a worker, run the following command:

```sh
rq worker my-queue-name
```

or, with the settings file (see below):

```sh
rq worker -c settings --path deploy --path .
```

See [here](https://python-rq.org/docs/workers/) for more information on how to configure workers. 
You may need to set the env for the session before execution if the underlying code depends on specific environment variables.


### Using with Piano Flask apps running supervisor

Create _settings.py_ file in your repo's _deploy_ directory. Copy/paste the following:

```python
import os

# see https://python-rq.org/docs/workers/#using-a-config-file

HOST = os.getenv("PIANO_Q_HOST", "piano-redis")
PORT = os.getenv("PIANO_Q_PORT", "6379")
REDIS_URL = 'redis://{host}:{port}'.format(host=HOST, port=PORT)

# You can also specify the Redis DB to use
# REDIS_HOST = 'redis.example.com'
# REDIS_PORT = 6380
# REDIS_DB = 3
# REDIS_PASSWORD = 'very secret'

# Queues to listen on
QUEUES = [os.getenv("PIANO_Q_NAME", "my-queue-name")]

# If you're using Sentry to collect your runtime exceptions, you can use this
# to configure RQ for it in a single step
# The 'sync+' prefix is required for raven: https://github.com/nvie/rq/issues/350#issuecomment-43592410
# SENTRY_DSN = 'sync+http://public:secret@example.com/1'

# If you want custom worker name
# NAME = 'my-worker-name'
```

In your supervisor-app.conf file (usually in the repo's _deploy_ directory), add the following:

```
[program:piano-q-worker]
; Point the command to the specific rq command you want to run.
; If you use virtualenv, be sure to point it to
; /path/to/virtualenv/bin/rq
; Also, you probably want to include a settings module to configure this
; worker.  For more info on that, see http://python-rq.org/docs/workers/
command=rq worker --path deploy --path . -c settings
; process_num is required if you specify >1 numprocs
process_name=%(program_name)s-%(process_num)s

; If you want to run more than one worker instance, increase this
numprocs=1

; This is the directory from which RQ is ran. Be sure to point this to the
; directory where your source code is importable from
directory=/home/docker/code

; RQ requires the TERM signal to perform a warm shutdown. If RQ does not die
; within 10 seconds, supervisor will forcefully kill it
stopsignal=TERM

; These are up to you
autostart=true
autorestart=true
```

You can rename the "piano-q-worker" to whatever name you'd like, it will be suffixed by a number to identify it when 
there are multiple instances of this worker.

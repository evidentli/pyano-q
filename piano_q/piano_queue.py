from typing import Union, overload
from abc import abstractmethod, ABCMeta
from typing import Callable

from enum import Enum

class JobStatus(Enum):
    queued = "queued"
    scheduled = "scheduled"
    started = "started"
    finished = "finished"
    failed = "failed"
    canceled = "canceled"
    deferred = "deferred"
    stopped = "stopped"


class PianoQ(metaclass=ABCMeta):

    q = None

    def __init__(self, name, host, port, default_timeout=300, default_result_expiry=500, testing=False, **kwargs):
        """
        Piano Queue Abstract Class
        :param name: name of queue
        :param host: broker/queue host name
        :param port: broker/queue port
        :param default_timeout: (optional, default=300) execution timeout, in seconds, for each job
        :param default_result_expiry: (optional, default=500) execution result expiry, in seconds, for each job
        :param testing: (optional, default=False) mock the queue (will work in synchronous mode)
        :param kwargs:
        """
        self.name = name
        self.host = host
        self.port = port
        self.default_timeout = default_timeout
        self.default_result_expiry = default_result_expiry
        self.testing = testing
        self._init_q(**kwargs)

    @abstractmethod
    def _init_q(self, **kwargs):
        """
        Initialise q
        :return:
        """
        pass

    @abstractmethod
    def empty_queue(self):
        """
        Empty the queue of all jobs
        :return:
        """
        pass

    @abstractmethod
    def delete_queue(self):
        """
        Delete the queue (as well as any jobs in the queue)
        :return:
        """
        pass

    @abstractmethod
    def enqueue(self, function, job_id=None, timeout=None, description: str = None, meta: dict = None, on_success: Callable = None, on_failure: Callable = None, depends_on = None, *args, **kwargs):
        """
        Add job to the queue
        :param function: function to execute
        :param job_id: (optional) unique job identifier
        :param timeout: (optional) execution timeout, in seconds, for this job
        :param description: (optional) job description
        :param meta: (optional) job metadata
        :param on_success: (optional) function to execute on job success
        :param on_failure: (optional) function to execute on job failure
        :param depends_on: (optional) specifies another job (or list of jobs) that must complete before this job will be queued
        :param args: arguments to pass to function when executed
        :param kwargs: keyword arguments to pass to function when executed
        :return: job_id if successful, False if job wasn't queued
        """
        pass

    @abstractmethod
    def enqueue_in(self, function, ttl, job_id=None, timeout=None, description: str = None, meta: dict = None, on_success: Callable = None, on_failure: Callable = None, depends_on = None, *args, **kwargs):
        """
        Add job to the queue to be executed in (ttl) seconds
        :param function: function to execute
        :param ttl: time in seconds to wait until executing function
        :param job_id: (optional) unique job identifier
        :param timeout: (optional) execution timeout, in seconds, for this job
        :param description: (optional) job description
        :param meta: (optional) job metadata
        :param on_success: (optional) function to execute on job success
        :param on_failure: (optional) function to execute on job failure
        :param depends_on: (optional) specifies another job (or list of jobs) that must complete before this job will be queued
        :param args: arguments to pass to function when executed
        :param kwargs: keyword arguments to pass to function when executed
        :return: job_id if successful, False if job wasn't queued
        """
        pass

    @abstractmethod
    def delete(self, job_id):
        """
        Delete a job
        :param job_id: identifier of job to delete
        :return:
        """
        pass

    @abstractmethod
    def cancel(self, job_id):
        """
        Cancel a job
        :param job_id: identifier of job to cancel
        :return:
        """
        pass

    @abstractmethod
    def kill(self, job_id):
        """
        Kill a job
        :param job_id: identifier of job to kill
        :return:
        """

    @abstractmethod
    def cancel_started_jobs_by_prefixes(self, prefixes: list) -> None:
        """
        Cancel job in queue if job is "started" and the job_id has a matching prefix
        :param prefixes: list of prefixes to identify valid job_ids, if empty match all
        :return:
        """
        pass

    @abstractmethod
    def get_status(self, job_id):
        """
        Get status of job using given job_id
        :param job_id: identifier of job to retrieve status for
        :return: status (see JobStatus)
        """
        pass

    @abstractmethod
    def is_pending(self, job_id) -> bool:
        """
        If job for given id is not yet complete (i.e. started or queued)
        :param job_id: identifier of job
        :return: bool
        """
        pass

    @abstractmethod
    def is_scheduled(self, job_id) -> bool:
        """
        If job for given id is scheduled to be executed
        :param job_id: identifier of job
        :return: bool
        """
        pass

    @abstractmethod
    def is_started(self, job_id) -> bool:
        """
        If job for given id is started and not yet complete
        :param job_id: identifier of job
        :return: bool
        """
        pass

    @abstractmethod
    def is_complete(self, job_id) -> bool:
        """
        If job for given id is complete
        :param job_id: identifier of job
        :return: bool
        """
        pass

    @abstractmethod
    def get_result(self, job_id):
        """
        Get result of job function execution
        :param job_id: identifier of job
        :return:
        """
        pass

    @abstractmethod
    def get_jobs(self, include_started: bool = False) -> list[dict]:
        """
        Get all jobs in the queue
        :return: list of jobs in queue
        """
        pass

    @abstractmethod
    @overload
    def store_document(self, name: str, key_or_mapping: str, value: Union[bytes, str, int, float, dict]) -> bool:
        """Store a single key value pair as a document"""
        ...

    @abstractmethod
    @overload
    def store_document(self, name: str, key_or_mapping: dict[str, Union[bytes, str, int, float, dict]]) -> bool:
        """Store multiple documents in a namespace, passed in as a dict of key value pairs"""
        ...
    
    @abstractmethod
    def store_document(self, name: str, key_or_mapping: Union[str, dict[str, Union[bytes, str, int, float, dict]]], value: Union[bytes, str, int, float, dict] = None) -> bool:
        """Store a document in a namespace"""
        pass

    @abstractmethod
    def get_namespace(self, name: str) -> dict:
        """Return all documents in a namespace"""
        pass

    @abstractmethod
    def expire_namespace(self, name: str, time: int) -> bool:
        """Set namespace to expire after a given number of sections"""
        pass

    @abstractmethod
    def clear_namespace(self, name: str) -> bool:
        """Delete all documents from a given namespace"""
        pass

    @abstractmethod
    def cancel_executing_job_by_id(self, job_id: str) -> bool:
        """Cancel a job given its job id"""
        pass

    @abstractmethod
    def get_job_args(self, job_id: str) -> tuple:
        """Return a tuple containing the arguments given to a job"""


from piano_q.redis_queue import RedisQ


def get_q(name, host=None, port=None, default_timeout=300, default_result_expiry=500, testing=False, **kwargs):
    """
    Get Piano Q object
    :param name: name of queue
    :param host: host name of piano q server
    :param port: host name of piano q port
    :param default_timeout: (optional, default=300) execution timeout, in seconds, for each job
    :param default_result_expiry: (optional, default=500) execution result expiry, in seconds, for each job
    :param testing: (optional, default=False) mock the queue (will work in synchronous mode)
    :param kwargs:
    :return: PianoQ
    """
    return RedisQ(name, host, port,
                  default_timeout=default_timeout,
                  default_result_expiry=default_result_expiry,
                  testing=testing,
                  **kwargs)

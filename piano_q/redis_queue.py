import json
import traceback
from typing import Union, overload, Callable
from datetime import timedelta

from redis import Redis
from rq import Queue
from rq.registry import StartedJobRegistry
from rq.job import Job, JobStatus as _JobStatus
from fakeredis import FakeStrictRedis
from rq.command import send_stop_job_command, send_kill_horse_command
from rq.worker import Worker

from piano_q.piano_queue import PianoQ, JobStatus

OnSuccess = Callable[[Job, Redis, any, ...], any]
OnFailure = Callable[[Job, Redis, any, any, any], any]


class RedisQ(PianoQ):

    def __init__(self, name, host, port, **kwargs):
        super().__init__(name, host, port, **kwargs)

    def _init_q(self, **kwargs):
        connection_kwargs = {}
        if self.host:
            connection_kwargs["host"] = self.host
        if self.port:
            connection_kwargs["port"] = self.port
        connection = Redis(**connection_kwargs)
        options = dict(default_timeout=self.default_timeout)
        kwargs.update(options)
        if self.testing:
            connection = FakeStrictRedis()
            kwargs.update(dict(is_async=False))
        self.q = Queue(self.name, connection=connection, **kwargs)
        self.q_started = StartedJobRegistry(self.name, connection)

    def _fetch_job(self, job_id) -> Union[Job, None]:
        try:
            return self.q.fetch_job(job_id)
        except Exception as e:
            print(traceback.format_exc())
            return None

    def empty_queue(self) -> bool:
        try:
            self.q.empty()
        except Exception as e:
            print(traceback.format_exc())
            return False
        return True

    def delete_queue(self) -> bool:
        try:
            self.q.delete(delete_jobs=True)
        except Exception as e:
            print(traceback.format_exc())
            return False
        return True

    def enqueue(self, function, job_id=None, timeout=None, description: str = None, meta: dict = None, on_success: OnSuccess = None, on_failure: OnFailure = None, depends_on = None, *args, **kwargs):
        if job_id:
            kwargs["job_id"] = job_id
        kwargs["result_ttl"] = kwargs.get("timeout", self.default_result_expiry)
        if description:
            kwargs["description"] = description
        if meta:
            kwargs["meta"] = meta
        if on_success:
            kwargs['on_success'] = on_success
        if on_failure:
            kwargs['on_failure'] = on_failure
        if depends_on:
            kwargs['depends_on'] = depends_on
        try:
            job = self.q.enqueue(function, *args, **kwargs)
        except Exception as e:
            print(traceback.format_exc())
            return False
        return job.id if job else False
    
    def enqueue_in(self, function, ttl, job_id=None, description: str = None, meta: dict = None, on_success: OnSuccess = None, on_failure: OnFailure = None, depends_on = None, *args, **kwargs):
        if job_id:
            kwargs["job_id"] = job_id
        kwargs["result_ttl"] = kwargs.get("timeout", self.default_result_expiry)
        if description:
            kwargs["description"] = description
        if meta:
            kwargs["meta"] = meta
        if on_success:
            kwargs['on_success'] = on_success
        if on_failure:
            kwargs['on_failure'] = on_failure
        if depends_on:
            kwargs['depends_on'] = depends_on
        try:
            job = self.q.enqueue_in(timedelta(seconds=ttl), function, *args, **kwargs)
        except Exception as e:
            print(traceback.format_exc())
            return False
        return job.id if job else False

    def delete(self, job_id):
        job = self._fetch_job(job_id)
        if job:
            job.delete()

    def cancel(self, job_id):
        job = self._fetch_job(job_id)
        if job:
            job.cancel()

    def kill(self, job_id):
        job = self._fetch_job(job_id)
        if not job:
            return
        status = job.get_status()
        attempts_left = 10
        while attempts_left and status in [JobStatus.queued.value, JobStatus.scheduled.value, JobStatus.deferred.value]:
            self.cancel(job_id)
            status = job.get_status()
            attempts_left -= 1

        workers = Worker.all(self.q.connection)
        for worker in workers:
            current_job = worker.get_current_job()
            if current_job and current_job.id == job_id:
                send_kill_horse_command(worker.connection, worker.name)

    def cancel_started_jobs_by_prefixes(self, prefixes=None):
        if prefixes is None:
            prefixes = []
        try:
            for job_id in self.q_started.get_job_ids():
                prefix_match = not prefixes or job_id.split("-")[0] in prefixes
                if prefix_match and self.is_started(job_id):
                    self.cancel(job_id)
        except Exception as e:
            print(traceback.format_exc())

    def get_status(self, job_id):
        job = self._fetch_job(job_id)
        if job:
            status = job.get_status()
            if status == _JobStatus.QUEUED.value:
                return JobStatus.queued.value
            elif status == _JobStatus.SCHEDULED.value:
                return JobStatus.scheduled.value
            elif status == _JobStatus.STARTED.value:
                return JobStatus.started.value
            elif status == _JobStatus.FINISHED.value:
                return JobStatus.finished.value
            elif status == _JobStatus.FAILED.value:
                return JobStatus.failed.value
            elif status == _JobStatus.CANCELED.value:
                return JobStatus.canceled.value
            elif status == _JobStatus.DEFERRED.value:
                return JobStatus.deferred.value
            elif status == _JobStatus.STOPPED.value:
                return JobStatus.stopped.value
        return None

    def is_pending(self, job_id):
        return self.get_status(job_id) in [JobStatus.started.value, JobStatus.queued.value, JobStatus.scheduled.value, JobStatus.deferred.value]

    def is_scheduled(self, job_id):
        return self.get_status(job_id) == JobStatus.scheduled.value

    def is_started(self, job_id):
        return self.get_status(job_id) == JobStatus.started.value

    def is_complete(self, job_id):
        return self.get_status(job_id) == JobStatus.finished.value

    def get_result(self, job_id):
        job = self._fetch_job(job_id)
        if job:
            return job.result
        return None

    def get_jobs(self, include_started=False):
        jobs = []
        queues = [self.q]
        if include_started:
            queues.append(self.q_started)
        try:
            for q in queues:
                for job_id in q.get_job_ids():
                    job = self.q.fetch_job(job_id)
                    jobs.append({
                        "id": job_id,
                        "status": job.get_status(),
                        "args": job.args,
                    })
        except Exception as e:
            print(traceback.format_exc())
        return jobs


    @overload
    def store_document(self, name: str, key_or_mapping: str, value: Union[bytes, str, int, float, dict]) -> bool:
        """Store a single key value pair as a document"""
        ...

    @overload
    def store_document(self, name: str, key_or_mapping: dict[str, Union[bytes, str, int, float, dict]]) -> bool:
        """Store multiple documents in a namespace, passed in as a dict of key value pairs"""
        ...
    
    def store_document(self, name: str, key_or_mapping: Union[str, dict[str, Union[bytes, str, int, float, dict]]], value: Union[bytes, str, int, float, dict] = None) -> bool:
        try:
            redis: Redis = self.q.connection
            if isinstance(key_or_mapping, str) and value is not None:
                # using the classic key, value pairs
                if isinstance(value, dict):
                    value = json.dumps(value)
                redis.hset(name, key_or_mapping, value)
            elif isinstance(key_or_mapping, dict) and value is None:
                key_or_mapping_dumped = {k: json.dumps(v) if isinstance(v, dict) else v for k, v in key_or_mapping.items()}
                redis.hset(name, None, None, key_or_mapping_dumped)
            # redis.hset returns the number of new documents, not 
            # including modified ones so it's not useful for making sure
            # everything went okay. So just assume here it was fine
            # and if not it'll be caught by the exception
            return True
        except Exception as e:
            print(traceback.format_exc())
            return False
    
    def get_namespace(self, name: str) -> dict:
        # Get all items, then decode the key and values, then try to 
        # JSON parse the values - if successful add that to the dict,
        # otherwise add the non JSON-parsed value to the dict.
        try:
            redis: Redis = self.q.connection
            _results = redis.hgetall(name)
            results = {}
            for k, v in _results.items():
                if (isinstance(k, bytes)):
                    k = k.decode()
                if isinstance(v, bytes):
                    v = v.decode()
                try:
                    v = json.loads(v)
                except json.decoder.JSONDecodeError:
                    pass
                results[k] = v
            return results
        except Exception as e:
            print(traceback.format_exc())
            return {}

    def expire_namespace(self, name: str, time: int) -> bool:
        try:
            redis: Redis = self.q.connection
            return redis.expire(name, time)
        except Exception as e:
            print(traceback.format_exc())
            return False
        
    def clear_namespace(self, name: str) -> bool:
        try:
            redis: Redis = self.q.connection
            redis.delete(name)
            return True # assume it worked if an exception wasn't raised
        except Exception as e:
            print(traceback.format_exc())
            return False
        
    def cancel_executing_job_by_id(self, job_id: str) -> bool:
        try:
            redis: Redis = self.q.connection
            send_stop_job_command(redis, job_id)
            return True
        except Exception as e:
            print(traceback.format_exc())
            return False
        
    def get_job_args(self, job_id: str) -> Union[tuple, None]:
        job = self._fetch_job(job_id)
        try:  
            if job:
                return job.args
            else:
                return None
        except Exception as e:
            print(traceback.format_exc())
            return None
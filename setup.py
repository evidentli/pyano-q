from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='piano_q',
    packages=find_packages(exclude=["tests"]),
    version='0.8.0',
    description='Piano Queue',
    author='Evidentli',
    author_email='infrastructure@evidentli.com',
    url='https://bitbucket.org/evidentli/pyano-q',
    license='BSD-3-Clause',
    keywords=['piano', 'util', 'queue', 'redis', 'broker'],
    classifiers=[
        "Programming Language :: Python :: 3.0"
    ],
    install_requires=requirements,
    python_requires='>=3.9'
)
